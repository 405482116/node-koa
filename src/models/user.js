import sequelize from "../../config/db";
import { DataTypes } from 'sequelize';

const User = sequelize.define("User", {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    username: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    token: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
});

export default User;