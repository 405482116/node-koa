import jwtFn from 'koa-jwt';
import { secret } from "../secret";
const jwt = jwtFn({ secret });

export default jwt;