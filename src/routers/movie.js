import Router from "koa-router";
import movie from "../service/movie";
const router = new Router();
router.get('/movie', async ctx => {
    const { kw } = ctx.query;
    let data = await movie.search(kw);
    ctx.body = data
})
export default router;