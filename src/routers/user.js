import { sign } from 'jsonwebtoken';
import Router from 'koa-router';
import admin from '../middleware/admin';
import jwt from '../middleware/jwt';
import { secret } from '../secret';
const user = new Router();
const detail = new Router();


detail.get('/info', async ctx => {
    ctx.body = { username: ctx.state.user.username };
});
detail.get('/admin', admin, async ctx => {
    ctx.body = { username: ctx.state.user.username };
})
user.post('/login', async ctx => {
    const { username } = ctx.request.body;
    if (username) {
        const token = sign({ username }, secret, { expiresIn: '1h' });
        ctx.body = {
            message: 'Get token success.',
            code: 0,
            token
        }
    } else {
        ctx.body = {
            message: 'Miss username',
            code: -1
        }
    }
})
user.use('/user', jwt, detail.routes(), detail.allowedMethods());

export default user;