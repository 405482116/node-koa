import Koa from "koa";
import koaBody from 'koa-body';
import user from './routers/user'
import movie from "./routers/movie";

const app = new Koa();

app.use(koaBody());
app
    .use(user.routes())
    .use(user.allowedMethods());
app
    .use(movie.routes())
    .use(movie.allowedMethods());
app.listen(3030, () => {
    console.log('Server started.1');
})