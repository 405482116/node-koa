
import { request } from 'http';
import querystring from "query-string";
export default {
    search: async (kw = '') => new Promise((resolve, reject) => {
        request({
            hostname: 'm.maoyan.com',
            path: `/ajax/search?${querystring.stringify({ kw, cityId: 10 })}`
        }, (res) => {
            res.setEncoding('utf-8');
            let data = [];
            res
                .on('data', (chunk) => {
                    console.log(112312, chunk);
                    data.push(chunk);
                })
                .on('end', () => {
                    console.log(112312, data);
                    resolve(data.join(''));
                })
        }).end();
    })
}